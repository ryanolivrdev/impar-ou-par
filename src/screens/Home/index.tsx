import { useState } from 'react'
import { Alert, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { generateRandomNumber } from '../../helpers/generateRandomNumber'
import { testNumberEven } from '../../helpers/testNumberEven'
import { styles } from './styles'

export function Home() {
  const [textInput, setTextInput] = useState('')
  const [result, setResult] = useState('')

  function handleStartGame(choice: string) {
    if (!textInput) {
      return Alert.alert('Error', 'E preciso digitar algo')
    }

    setResult('')
    setTextInput('')

    const userNumber = Number(textInput)
    const userChoice = choice
    const cpuChoice = generateRandomNumber()

    const isEvens = testNumberEven(userNumber, cpuChoice)

    if (userChoice === 'Evens') {
      if (isEvens) {
        setResult('O jogador venceu o jogo')
      } else if (!isEvens) {
        setResult('A CPU venceu o jogo')
      }
    }

    if (userChoice === 'Odds') {
      if (!isEvens) {
        setResult('O jogador venceu o jogo')
      } else if (isEvens) {
        setResult('A CPU venceu o jogo')
      }
    }
  }

  return (
    <View style={styles.homeContainer}>
      <Text style={styles.title}>Par ou ímpar</Text>
      <Text style={styles.subtitle}>tente vencer a CPU</Text>
      <TextInput
        style={styles.input}
        value={textInput}
        onChangeText={setTextInput}
        keyboardType="numeric"
        placeholderTextColor="#B2B2B2"
        selectionColor="#666666"
        placeholder="Digite um numero"
        returnKeyType="done"
      />
      <TouchableOpacity
        onPress={() => handleStartGame('Evens')}
        style={styles.button}
      >
        <Text style={styles.textButton}>vai dar par</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => handleStartGame('Odds')}
        style={styles.button}
      >
        <Text style={styles.textButton}>vai dar impar</Text>
      </TouchableOpacity>
      <Text style={styles.subtitle}>{result}</Text>
    </View>
  )
}
